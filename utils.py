from django.contrib.auth.models import User

from .models import Settings


def get_user_avatar(user):
    """
    This is a mock function to get user avatars. It needs to be modified to get real user avatar
    """
    return 'https://via.placeholder.com/60x60'


def get_user_department(user):
    """
    This is a mock function to get user department. It needs to be modified to get real information
    """
    return 'Test'


def check_user_roles(user, role):
    """
    This is a mock function for calling roles. Change it in the production
    """
    un = user.username
    if un[0] in ('k', 'a'):
        role_set = (
            'can_vote_topic', 'can_vote_comment', 'can_comment',
            'can_create_topic', 'can_create_group',
            'can_delete_topic', 'can_delete_group', 'can_delete_comment',
            'can_edit_topic', 'can_edit_group', 'can_edit_comment',
            'can_control_season', 'can_approve_topic',
        )
    else:
        role_set = (
            'can_vote_topic', 'can_vote_comment', 'can_comment',
        )
    if role in role_set:
        return True
    else:
        return False


def check_is_admin(user):
    role_set = (
        'can_create_topic', 'can_create_group',
        'can_delete_topic', 'can_delete_group', 'can_delete_comment',
        'can_edit_topic', 'can_edit_group', 'can_edit_comment',
        'can_control_season', 'can_approve_topic',
    )
    for role in role_set:
        if check_user_roles(user, role):
            return True


def is_season_active():
    settings = Settings.objects.first()
    return settings.can_vote


def get_vote_limit(user):
    """
    This is a mock function for calling vote limit.
    """
    return 10


def get_user_model():
    return User
