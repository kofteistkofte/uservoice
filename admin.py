from django.contrib import admin
from .models import Topic, Group, Image, File, Comment, Settings


class SingletonAdmin(admin.ModelAdmin):
    actions = None

    def has_add_permission(self, request):
        permission = super().has_add_permission(request)
        if permission and not self.model.objects.count():
            return True
        return False

    def has_delete_permission(self, request, obj=None):
        return False


class ImageInline(admin.TabularInline):
    model = Image
    extra = 1


class FileInline(admin.TabularInline):
    model = File
    extra = 1


class CommentInline(admin.TabularInline):
    model = Comment


@admin.register(Topic)
class TopicAdmin(admin.ModelAdmin):
    inlines = [ImageInline, FileInline, CommentInline]


@admin.register(Comment)
class CommentAdmin(admin.ModelAdmin):
    pass


@admin.register(Group)
class GroupAdmin(admin.ModelAdmin):
    pass


@admin.register(Settings)
class SettingsAdmin(SingletonAdmin):
    pass


@admin.register(File)
class FileAdmin(admin.ModelAdmin):
    pass


@admin.register(Image)
class ImageAdmin(admin.ModelAdmin):
    fields = ['pk', 'title', 'topic', 'image']
    readonly_fields = ['pk']
