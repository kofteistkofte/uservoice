from django.urls import path

from . import views

app_name = 'vote'


urlpatterns = [
    path('', views.IndexView.as_view(), name='index'),
    path('admin/', views.AdminView.as_view(), name='admin'),
    path('season_update/', views.season_update_view, name='season_update'),
    path('topic_create/', views.TopicCreateView.as_view(), name='topic_create'),
    path('topic/<slug:slug>/', views.TopicDetailView.as_view(), name='topic_detail'),
    path('topic/<slug:slug>/vote/', views.topic_vote_view, name='topic_vote'),
    path('topic/<slug:slug>/edit/', views.TopicUpdateView.as_view(), name='topic_edit'),
    path('topic/<slug:slug>/delete/', views.topic_delete_view, name='topic_delete'),
    path('topic/<slug:slug>/approve/', views.topic_approve_view, name='topic_approve'),
    path('topic/<slug:slug>/toggle_comment/', views.topic_toggle_comment_view, name='topic_toggle_comment'),
    path('add_comment/', views.CommentCreateView.as_view(), name='comment'),
    path('group_create/', views.GroupCreateView.as_view(), name='group_create'),
    path('group/<int:pk>/', views.GroupView.as_view(), name='group'),
    path('group/<int:pk>/edit/', views.GroupEditView.as_view(), name='group_edit'),
    path('group/<int:pk>/delete/', views.group_delete_view, name='group_delete'),
    path('user_history/', views.UserHistoryView.as_view(), name='user_history'),
    path('approved_topics/', views.ApprovedTopicsView.as_view(), name='approved_topics'),
    path('upload/<file_type>/', views.upload_view, name='upload'),
]
