from django import forms

from .models import Topic, File, Image


class FileForm(forms.ModelForm):
    title = forms.CharField(required=True)

    class Meta:
        model = File
        fields = ['title']

    def clean_id(self):
        id = self.cleaned_data['id']
        return id


class ImageForm(forms.ModelForm):
    title = forms.CharField(required=True)

    class Meta:
        model = Image
        fields = ['title']

    def clean_id(self):
        id = self.cleaned_data['id']
        return id


class TopicForm(forms.ModelForm):
    class Meta:
        model = Topic
        fields = ['group', 'title', 'desc', 'body']
    
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields['group'].widget.attrs.update({'class': 'custom-select mr-sm-2'})


ImageFormSet = forms.inlineformset_factory(Topic, Image, form=ImageForm, extra=1)
FileFormSet = forms.inlineformset_factory(Topic, File, form=FileForm, extra=1)
