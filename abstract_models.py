from itertools import count

from django.utils.text import slugify
from django.db import models
from django.conf import settings
from django.utils.translation import gettext as _


User = settings.AUTH_USER_MODEL


class AbstractVote(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE,
                             related_name='%(class)s_vote', verbose_name=_('User'))

    class Meta:
        abstract = True

    def save(self, *args, **kwargs):
        if not self.pk:
            self.topic.vote_count += 1
            self.topic.save()
        super().save(*args, **kwargs)

    def delete(self, *args, **kwargs):
        topic = self.topic
        topic.vote_count += -1
        topic.save()
        super().delete(*args, **kwargs)


class SingletonModel(models.Model):
    """ An abstract model for forcing models to be singleton. """
    def save(self, *args, **kwargs):
        self.pk = 1
        super(SingletonModel, self).save(*args, **kwargs)

    @classmethod
    def get_object(cls):
        obj, created = cls.objects.get_or_create(pk=1)
        return obj

    class Meta:
        abstract = True


def slug_generator(model, value, slug_field_name='slug', max_length=25):
    """
    A unique slug generation script for models. Only 'model' and 'value' arguments are requires.
    Args:
        model = Model class. If you use in 'save' method, you can use as 'self.__class__'
        value = The string that your slug is going to be based on
        slug_field_name = name of the slug field of model. Default is 'slug'
        max_length = Maximum lenght of your slug. Default is '25'

    Example usage with 'title' field as base field:
        def save(self, *args, **kwargs):
            if not self.slug:
                self.slug = slug_generator(self.__class__, self.title)
            super().save(*args, **kwargs)
    """

    slug_candidate = slug_original = slugify(value, allow_unicode=False)

    for i in count(1):
        if not model.objects.filter(**{slug_field_name: slug_candidate}):
            return slug_candidate
        else:
            slug_candidate = '{}-{}'.format(slug_original, i)
