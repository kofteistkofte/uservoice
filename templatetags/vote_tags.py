from django import template
from ..models import Group, Settings
from ..utils import (
        check_user_roles, get_vote_limit, get_user_avatar, get_user_department, check_is_admin, is_season_active
)

register = template.Library()


@register.simple_tag
def get_groups():
    groups = Group.objects.all()
    return groups


@register.simple_tag
def vote_session():
    settings = Settings.objects.first()
    return settings.can_vote


@register.filter
def get_avatar(user):
    return get_user_avatar(user)


@register.filter
def get_department(user):
    get_user_department(user)


@register.filter
def check_role(user, role):
    return check_user_roles(user, role)


@register.filter
def is_admin(user):
    return check_is_admin(user)


@register.filter
def get_remaining_votes(user):
    vote_limit = get_vote_limit(user)
    user_votes = user.topicvote_vote.count()
    return vote_limit - user_votes


@register.simple_tag
def get_tinymce_key():
    return 'qxcj9am71bspf5q6qw4483965q41fwa6q3x9qeij9930puj4'


@register.simple_tag
def get_season_active():
    return is_season_active()
