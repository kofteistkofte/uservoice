import json

from django.views import generic
from django.urls import reverse_lazy
from django.http import HttpResponseRedirect, HttpResponse
from django.core.exceptions import ObjectDoesNotExist
from django.contrib.auth.mixins import LoginRequiredMixin, UserPassesTestMixin
from django.contrib.auth.decorators import user_passes_test, login_required

from .models import Topic, TopicVote, Comment, Group, Settings, File, Image
from .view_utils import (
        TopicMixin, user_vote_test, delete_group_test, season_control_test,
        delete_topic_test, approve_topic_test
)
from .utils import check_user_roles, get_vote_limit, get_user_model, check_is_admin


User = get_user_model()


class IndexView(LoginRequiredMixin, generic.ListView):
    model = Topic
    template_name = 'vote/index.html'


class UserHistoryView(IndexView):
    def get_queryset(self):
        current_user_id = self.request.user.id
        return Topic.objects.filter(vote__user__pk=current_user_id)


class ApprovedTopicsView(IndexView):
    def get_queryset(self):
        return Topic.objects.filter(approved=True)


class TopicDetailView(LoginRequiredMixin, generic.DetailView):
    model = Topic
    template_name = 'vote/topic.html'
    sluf_field = 'slug'

    def get_context_data(self, *args, **kwargs):
        context = super().get_context_data(*args, **kwargs)
        obj = self.get_object()
        current_user_id = self.request.user.id
        user_ids = list(obj.comment.all().values_list('user__id', flat=True))
        if current_user_id not in user_ids:
            user_ids.append(current_user_id)
        context['users'] = User.objects.filter(id__in=user_ids)
        return context


@user_passes_test(user_vote_test, login_url=reverse_lazy('vote:index'))
def topic_vote_view(request, slug):
    vote_limit = get_vote_limit(request.user)
    topic = Topic.objects.get(slug=slug)
    if request.user.topicvote_vote.count() < vote_limit:
        try:
            vote, created = TopicVote.objects.get_or_create(
                    user=request.user, topic=topic)
            if not created or topic.approved:
                vote.delete()
        except ObjectDoesNotExist:
            pass
    else:
        try:
            vote = TopicVote.objects.get(
                    user=request.user, topic=topic)
            if vote:
                vote.delete()
        except ObjectDoesNotExist:
            pass
    try:
        return HttpResponseRedirect(request.GET['redirect'])
    except Exception:
        return HttpResponseRedirect(topic.get_absolute_url())


class TopicCreateView(LoginRequiredMixin, UserPassesTestMixin, TopicMixin, generic.CreateView):
    def test_func(self):
        return check_user_roles(self.request.user, 'can_create_topic')


class TopicUpdateView(LoginRequiredMixin, UserPassesTestMixin, TopicMixin, generic.UpdateView):
    def test_func(self):
        return check_user_roles(self.request.user, 'can_edit_topic')


@user_passes_test(delete_topic_test, login_url=reverse_lazy('vote:index'))
def topic_delete_view(request, slug):
    obj = Topic.objects.get(slug=slug)
    obj.delete()
    return HttpResponseRedirect(reverse_lazy('vote:index'))


@user_passes_test(approve_topic_test, login_url=reverse_lazy('vote:index'))
def topic_approve_view(request, slug):
    obj = Topic.objects.get(slug=slug)
    obj.approved = not obj.approved
    obj.save()
    return HttpResponse('Success')


@user_passes_test(approve_topic_test, login_url=reverse_lazy('vote:index'))
def topic_toggle_comment_view(request, slug):
    obj = Topic.objects.get(slug=slug)
    obj.can_comment = not obj.can_comment
    obj.save()
    return HttpResponse('Success')


class CommentCreateView(LoginRequiredMixin, UserPassesTestMixin, generic.CreateView):
    model = Comment
    template_name = 'vote/partial/comment_form.html'
    fields = ['topic', 'user', 'parent', 'body']

    def test_func(self):
        return check_user_roles(self.request.user, 'can_comment')


class GroupView(IndexView):
    def get_queryset(self):
        return Topic.objects.filter(group__pk=self.kwargs['pk'])

    def get_context_data(self, *args, **kwargs):
        context = super().get_context_data(*args, **kwargs)
        context['group'] = Group.objects.get(pk=self.kwargs['pk'])
        return context


class GroupCreateView(LoginRequiredMixin, UserPassesTestMixin, generic.CreateView):
    model = Group
    template_name = 'vote/group_form.html'
    fields = ['title', 'desc']

    def test_func(self):
        return check_user_roles(self.request.user, 'can_create_group')


class GroupEditView(LoginRequiredMixin, UserPassesTestMixin, generic.UpdateView):
    model = Group
    template_name = 'vote/group_form.html'
    fields = ['title', 'desc']

    def test_func(self):
        return check_user_roles(self.request.user, 'can_edit_group')


@user_passes_test(delete_group_test, login_url=reverse_lazy('vote:index'))
def group_delete_view(request, pk):
    obj = Group.objects.get(pk=pk)
    obj.delete()
    return HttpResponse('Deleted')


@user_passes_test(season_control_test, login_url=reverse_lazy('vote:index'))
def season_update_view(request):
    settings = Settings.objects.first()
    settings.can_vote = not settings.can_vote
    settings.save()
    return HttpResponse('Changed')


class AdminView(generic.TemplateView):
    template_name = 'vote/admin.html'

    def test_func(self):
        return check_is_admin(self.request.user)

    def get_context_data(self, *args, **kwargs):
        context = super().get_context_data(*args, **kwargs)
        context['groups'] = Group.objects.all()
        context['settings'] = Settings.objects.first()
        context['topics'] = Topic.objects.all()
        return context


@login_required
def upload_view(request, file_type):
    if request.method == 'POST':
        if file_type == 'file':
            file = request.FILES['upload']
            obj = File.objects.create(file=file)
            obj_url = obj.file.url
            obj_name = obj.file.name
        elif file_type == 'image':
            image = request.FILES['upload']
            obj = Image.objects.create(image=image)
            obj_url = obj.image.url
            obj_name = obj.image.name
        data = json.dumps({
            'url': obj_url,
            'pk': obj.id,
            'name': obj_name,
            'title': obj.title or '',
        })
        return HttpResponse(data, content_type='application/json')
    return HttpResponse('Unexpected Data')
