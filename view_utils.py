import random
import string

from django.http import HttpResponseRedirect

from .models import Topic, Settings
from .utils import check_user_roles
from .forms import TopicForm, ImageFormSet, FileFormSet


class TopicMixin():
    template_name = 'vote/topic_form.html'
    model = Topic
    form_class = TopicForm

    def get_context_data(self, *args, **kwargs):
        context = super().get_context_data(*args, **kwargs)
        context['random_key'] = ''.join(random.sample(string.ascii_letters + string.digits, 16))
        return context

    def get(self, request, *args, **kwargs):
        try:
            self.object = self.get_object()
        except Exception:
            self.object = None
        form_class = self.get_form_class()
        form = self.get_form(form_class)
        image_formset = ImageFormSet(instance=self.object)
        file_formset = FileFormSet(instance=self.object)
        return self.render_to_response(
                self.get_context_data(form=form,
                                      image_formset=image_formset,
                                      file_formset=file_formset))

    def post(self, request, *args, **kwargs):
        try:
            self.object = self.get_object()
        except Exception:
            self.object = None
        form_class = self.get_form_class()
        form = self.get_form(form_class)
        image_formset = ImageFormSet(self.request.POST or None, self.request.FILES or None, instance=self.object)
        file_formset = FileFormSet(self.request.POST or None, self.request.FILES or None, instance=self.object)
        if form.is_valid():
            return self.form_valid(form, image_formset, file_formset)
        else:
            return self.form_invalid(form, image_formset, file_formset)

    def form_valid(self, form, image_formset, file_formset):
        self.object = form.save()
        if file_formset.is_valid():
            for form in file_formset:
                try:
                    cd = form.cleaned_data
                    file = cd['id']
                    file.topic = self.object
                    file.title = cd['title']
                    file.save()
                except Exception:
                    pass
        if image_formset.is_valid():
            for form in image_formset:
                try:
                    cd = form.cleaned_data
                    image = cd['id']
                    image.topic = self.object
                    image.title = cd['title']
                    image.save()
                except Exception:
                    pass
        return HttpResponseRedirect(self.get_success_url())

    def form_invalid(self, form, image_formset, file_formset):
        return self.render_to_response(
            self.get_context_data(form=form,
                                  image_formset=image_formset,
                                  file_formset=file_formset))


def user_vote_test(user):
    settings = Settings.objects.first()
    if check_user_roles(user, 'can_vote_topic') and settings.can_vote:
        return True


def delete_group_test(user):
    return check_user_roles(user, 'can_delete_group')


def delete_topic_test(user):
    return check_user_roles(user, 'can_delete_topic')


def season_control_test(user):
    return check_user_roles(user, 'can_control_season')


def approve_topic_test(user):
    return check_user_roles(user, 'can_approve_topic')
