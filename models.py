from django.db import models
from django.urls import reverse
from django.conf import settings
from django.utils.translation import gettext as _

from .abstract_models import AbstractVote, SingletonModel, slug_generator


User = settings.AUTH_USER_MODEL


class Settings(SingletonModel):
    can_vote = models.BooleanField(_('Can Users Vote'), default=True)

    class Meta:
        verbose_name = _('Settings')
        verbose_name_plural = _('Settings')

    def __str__(self):
        return _('Settings')


class Group(models.Model):
    title = models.CharField(_('Group Name'), max_length=32)
    desc = models.CharField(_('Group Description'), max_length=1024, blank=True)

    class Meta:
        verbose_name = _('Group')
        verbose_name_plural = _('Groups')

    def __str__(self):
        return self.title

    def get_absolute_url(self):
        return reverse('vote:group', args=[self.pk])


class Topic(models.Model):
    group = models.ForeignKey(Group, on_delete=models.CASCADE,
                              related_name='topic', verbose_name=_('Group'))
    title = models.CharField(_('Title'), max_length=255)
    slug = models.SlugField(_('Slug'), max_length=255, blank=True, null=True)
    desc = models.CharField(_('Topic Description'), max_length=512)
    date = models.DateTimeField(_('Date'), auto_now_add=True, blank=True, null=True)
    body = models.TextField(_('Topic Body'), blank=True)
    approved = models.BooleanField(_('Is Approved'), default=False)
    can_comment = models.BooleanField(_('Can Comment'), default=True)
    vote_count = models.PositiveSmallIntegerField(_('Vote Count'), default=0)

    class Meta:
        verbose_name = _('Topic')
        verbose_name_plural = _('Topics')
        ordering = ['-date']

    def __str__(self):
        return self.title

    def save(self, *args, **kwargs):
        if not self.slug:
            self.slug = slug_generator(self.__class__, self.title)
        super().save(*args, **kwargs)

    def get_absolute_url(self):
        return reverse('vote:topic_detail', args=[self.slug])


class Comment(models.Model):
    topic = models.ForeignKey(Topic, on_delete=models.CASCADE,
                              related_name='comment', verbose_name=_('Topic'))
    user = models.ForeignKey(User, on_delete=models.CASCADE,
                             related_name='comment', verbose_name=_('User'))
    parent = models.ForeignKey('self', on_delete=models.CASCADE, null=True, blank=True,
                               related_name='comment', verbose_name=_('Parent'))
    body = models.TextField(_('Comment Body'))
    date = models.DateTimeField(_('Date'), auto_now_add=True, blank=True, null=True)
    vote_count = models.PositiveSmallIntegerField(_('Vote Count'), default=0)

    class Meta:
        verbose_name = _('Comment')
        verbose_name_plural = _('Comments')

    def __str__(self):
        return self.topic.title

    def get_absolute_url(self):
        return self.topic.get_absolute_url()


class File(models.Model):
    topic = models.ForeignKey(Topic, on_delete=models.CASCADE, null=True, blank=True,
                              related_name='file', verbose_name=_('Topic'))
    title = models.CharField(_('File Title'), max_length=64, null=True, blank=True)
    file = models.FileField(_('File'), upload_to='files/')
    random_key = models.CharField(_('Random Key'), max_length=16, blank=True, null=True)

    class Meta:
        verbose_name = _('File')
        verbose_name_plural = _('Files')

    def __str__(self):
        return self.title or 'Null'


class Image(models.Model):
    topic = models.ForeignKey(Topic, on_delete=models.CASCADE, null=True, blank=True,
                              related_name='image', verbose_name=_('Topic'))
    title = models.CharField(_('Image Title'), max_length=64, null=True, blank=True)
    image = models.ImageField(_('Image'), upload_to='images/')
    random_key = models.CharField(_('Random Key'), max_length=16, blank=True, null=True)

    class Meta:
        verbose_name = _('Image')
        verbose_name_plural = _('Images')

    def __str__(self):
        return self.title or 'Null'


class TopicVote(AbstractVote):
    topic = models.ForeignKey(Topic, on_delete=models.CASCADE,
                              related_name='vote', verbose_name=_('Topic'))

    class Meta:
        verbose_name = _('Topic Vote')
        verbose_name_plural = _('Topic Votes')
        unique_together = [['topic', 'user']]

    def __str__(self):
        return f'{self.topic} - {self.user}'


class CommentVote(AbstractVote):
    comment = models.ForeignKey(Comment, on_delete=models.CASCADE,
                                related_name='vote', verbose_name=_('Comment'))

    class Meta:
        verbose_name = _('Comment Vote')
        verbose_name_plural = _('Comment Votes')
        unique_together = [['comment', 'user']]

    def __str__(self):
        return f'{self.comment} - {self.user}'
